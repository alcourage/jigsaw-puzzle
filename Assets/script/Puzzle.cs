﻿using UnityEngine;
using System.Collections;

public class Puzzle : MonoBehaviour {

    private int pieceNum;
    private string[] piece;
    private Space space;
    private pieceSpace pieceSpace;

    // Use this for initialization
    void Start () {

        // spaceコンポーネントをシーン内から探して取得する
        space = FindObjectOfType<Space>();
        string buf;

        pieceNum = space.GetPieceNum();

        piece = new string[pieceNum];

        for (int i = 0; i < pieceNum; i++)
        {
            piece[i] = "piece";
            buf = i.ToString();
            piece[i] = piece[i] + buf;
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D c)
    {
        // レイヤー名を取得
        string layerName = LayerMask.LayerToName(c.gameObject.layer);

        // spaceコンポーネントをシーン内から探して取得する
        pieceSpace = FindObjectOfType<pieceSpace>();

        for (int i = 0; i < pieceNum; i++)
        {
            if (layerName == piece[i])
            {
               transform.position = pieceSpace.GetTransform();
            }
        }

    }
}
